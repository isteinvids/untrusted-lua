Written in Java, uses Lua as the programming language

Uses LuaJ
http://luaj.org/luaj/README.html

And RSyntaxTextArea
https://github.com/bobbylight/RSyntaxTextArea

Recreation of Untrusted originally by Alex Nisnevich and Greg Shuflin
https://github.com/AlexNisnevich/untrusted