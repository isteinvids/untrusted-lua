--[[
ayy lmao
--]]

function Main:init()
    level.setlevelstatus("Level 0");
    player.init(2, 3);
    level.settile(6, 2, "computer");
--]]--%START_EDIT%
    for i = 0, 22 do
        level.settile(i + 4, 4, "block");
        level.settile(i + 4, 14, "block");
    end

    for j = 0, 8 do
        level.settile(4, 5 + j, "block");
        level.settile(26, 5 + j, "block");
    end
--]]--%END_EDIT%
    level.settile(8, 8, "exit");
end


function Main:exit()
    if player.hasitem("computer") then
        return true;
    end
    level.setlevelstatus("Don't forget to pick up the computer!");
    return false;
end