function deadByWater()
    raft = level.getentity("raft");
    flag = true;
    if raft ~= nil then
        raftpos = raft.getposition();
        playerpos = player.getposition();
        if raftpos.x == playerpos.x and raftpos.y == playerpos.y then
            flag = false;
        end
    end
    if flag == true then
        player.killedBy("Drowned in water");
    end
end

function Main:init()
    level.setlevelstatus("Level 1");
    level.setbackground(0x8F4700);
    player.init(level.getwidth()/2, 1);
    
    local water = {}
    water.symbol = string.char(0x2592);
    water.colour = 0x0000FF; --[[ blue ]]--
    water.collidable = false;
    water.onTileCollision = deadByWater;
    level.addtileproperty("water", water);

    local rafttab = {}
    rafttab.symbol = '8';
    rafttab.colour = 0xEE7700;
    rafttab.position = {level.getwidth()/2, 4};
    rafttab.collidable = false;
    rafttab.behavior = entityTick
    level.addentity("raft", rafttab);

    
    local offx = 0;
    local offy = 4;
    for i = 0, 32 do
        for j = 0, 15 do
            level.settile(offx+i, offy+j, "water");
        end
    end

    level.settile(6, level.getheight() - 6, "exit");
end

function entityTick(entity)--]]--%START_EDIT%
    entity.move("down");
--]]--%END_EDIT%
end

function Main:validate()
    level.validateExactlyXManyObjects(1, "exit");
    level.validateExactlyXManyObjects(528, "water");
end
