The API:

level.reset()
    Resets the level so that it's empty

level.addentity(String id, Table properties)
    Adds an entity to the level.
    Properties:
        symbol: Character
        colour: Integer
        position: Table, {x, y}
        collidable: Boolean
    Events:
        behavior
            Arguments: Entity
        onEntityCollision
            Arguments: Entity

level.getentity(String id)
    Returns the entity
    
level.additemtype(String name, Table properties)
    Adds an item type to the level. Player can get item
    type first by placing it in the level via level.settile
    Properties:
        symbol: Character
        colour: Integer

level.addtileproperty(String name, Table properties)
    Adds a new tile property
    Properties:
        symbol: Character
        colour: Integer
        collidable: Boolean or LuaFunction returning true or false

level.getsize()
    Returns width, height

level.getwidth()
    Returns width

level.getheight()
    Returns height

level.gettile(int x, int y)
    Gets the tile at x, y

level.settile(int x, int y, String tile)
    Sets the tile at x, y to tile ONLY if there's air at the position (prevents some cheating)

level.setlevelstatus(String status)
    Sets level status text

level.resetlevelstatus()
    Resets level status text

level.setbackground(int colour)
    Sets background colour

level.validateAtLeastXObjects(String name, int amount)
    TODO

level.validateExactlyXManyObjects(String name, int amount)
    TODO

level.maze(int width, int height)
    Generates a maze