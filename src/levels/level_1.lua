--[[
level 2
--]]

function Main:init()
    level.setlevelstatus("Level 2");
    player.init(5, 5);

    level.settile(25, 23, "exit");

--]]--%START_EDIT%

--]]--%END_EDIT%

    w,h = level.getsize();

    maze = prim.generatemaze(w,h);
    for i = 1, w do
        str = "";
        for j = 1, h do
            str =  str .. maze[i][j];
                if maze[i][j] == 1 then
                    if i ~= 5 or j ~= 5 then
                        level.settile(i, j, "block");
                    end
                end
            end
    end

    level.settile(25+1, 23, "block");
    level.settile(25-1, 23, "block");
    level.settile(25, 23+1, "block");
    level.settile(25, 23-1, "block");
end