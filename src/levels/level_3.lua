--[[
ayy lmao
--]]

--]]--%START_EDIT%
function Main:init()
    width, height = level.getsize();

    level.setlevelstatus("Level 3");
    player.init(width-2,height-2);

    for i = 0, 22 do
        level.settile(i + 4, 4, "block");
        level.settile(i + 4, 14, "block");
    end

    for j = 0, 8 do
        level.settile(4, 5+j, "block");
        level.settile(26, 5+j, "block");
    end

    level.settile(0, 0, "exit");

    entitytable = {};
    entitytable.symbol = 'O';
    entitytable.position = {width/2, height/2};
    entitytable.colour = 0x333333;
    entitytable.collidable = true;
    entitytable.behavior = entityevent;
    entitytable.onEntityCollision = steppedOn;

    level.addentity("bot", entitytable, entityevent);
end

function entityevent(me)
    direct = moveToward(me.getposition(), player.getposition());
    me.move(direct);
  --  me.test();
end

function moveToward(from, to)
    leftDist = from.x - to.x;
    upDist = from.y - to.y;

    direction = "";
    if upDist == 0 and leftDist == 0 then
        return "none";
    end
    if upDist > 0 and upDist >= leftDist then
        direction = "up";
    elseif upDist < 0 and upDist < leftDist then
        direction = "down";
    elseif leftDist > 0 and leftDist >= upDist then
        direction = "left";
    else
        direction = "right";
    end
    return direction;
end

function steppedOn(me)
    player.killedBy("whoopsies");
end

--]]--%END_EDIT%

function Main:validate()
    level.validateExactlyXManyObjects(64, "block");
    level.validateExactlyXManyObjects(1, "exit");
end
