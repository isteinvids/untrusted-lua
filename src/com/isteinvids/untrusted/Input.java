package com.isteinvids.untrusted;

import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;
import java.util.ArrayList;
import java.util.concurrent.CopyOnWriteArrayList;

/**
 *
 * @author EmirRhouni
 */
public class Input implements KeyListener, MouseListener, MouseMotionListener {

    private final CopyOnWriteArrayList<Integer> keys;
    private final CopyOnWriteArrayList<Integer> mouse;
    private int mouseX = 0;
    private int mouseY = 0;
    private final boolean[] keysPressedTemp;
    private final boolean[] mousePressedTemp;
    private final Runnable run;
    private final ArrayList<InputTextEvent> events;

    public interface InputTextEvent {

        public void textEnteredEvent(char c);
    }

    public Input(Runnable run) {
        this.run = run == null ? new Runnable() {
            @Override
            public void run() {
            }
        } : run;
        events = new ArrayList();
        keys = new CopyOnWriteArrayList();
        mouse = new CopyOnWriteArrayList();
        keysPressedTemp = new boolean[256];
        for (int i = 0; i < keysPressedTemp.length; i++) {
            keysPressedTemp[i] = false;
        }
        mousePressedTemp = new boolean[32];
        for (int i = 0; i < mousePressedTemp.length; i++) {
            mousePressedTemp[i] = false;
        }
    }

    public boolean isKeyPressed(int key) {
        if (!keysPressedTemp[key] && isKeyDown(key)) {
            keysPressedTemp[key] = true;
            return true;
        } else if (!isKeyDown(key)) {
            keysPressedTemp[key] = false;
            return false;
        }
        return false;
    }

    public boolean isMousePressed(int mouse) {
        if (!mousePressedTemp[mouse] && isMouseDown(mouse)) {
            mousePressedTemp[mouse] = true;
            return true;
        } else if (!isMouseDown(mouse)) {
            mousePressedTemp[mouse] = false;
            return false;
        }
        return false;
    }

    @Override
    public void keyTyped(KeyEvent e) {
        run.run();
    }

    @Override
    public void keyPressed(KeyEvent e) {
        if (!isKeyDown(e.getKeyCode())) {
            keys.add(e.getKeyCode());
        }
//        if ((!e.isActionKey() && !e.isAltDown() && !e.isControlDown() && 
        if (e.getKeyCode() != KeyEvent.VK_CONTROL && e.getKeyCode() != KeyEvent.VK_SHIFT && e.getKeyCode() != KeyEvent.VK_ALT
                && e.getKeyCode() != KeyEvent.VK_NUM_LOCK && e.getKeyCode() != KeyEvent.VK_CAPS_LOCK) {
            runEvent(e.getKeyChar());
        }
        run.run();
    }

    @Override
    public void keyReleased(KeyEvent e) {
        removeFromKeys(e.getKeyCode());
        run.run();
    }

    public boolean isKeyDown(int key) {
        for (int i = 0; i < keys.size(); i++) {
            if (keys.get(i).equals(key)) {
                return true;
            }
        }
        return false;
    }

    public void removeFromKeys(int key) {
        for (int i = 0; i < keys.size(); i++) {
            if (keys.get(i).equals(key)) {
                keys.remove(i);
            }
        }
    }

    public boolean isMouseDown(Integer mkey) {
        for (int i = 0; i < mouse.size(); i++) {
            if (mouse.get(i).equals(mkey)) {
                return true;
            }
        }
        return false;
    }

    public void removeFromMouse(Integer mkey) {
        for (int i = 0; i < mouse.size(); i++) {
            if (mouse.get(i).equals(mkey.intValue())) {
                mouse.remove(i);
            }
        }
    }

    @Override
    public void mouseClicked(MouseEvent e) {
        if (!isMouseDown(e.getButton())) {
//            mouse.add(e.getButton());
        }
        run.run();
    }

    @Override
    public void mousePressed(MouseEvent e) {
        if (!isMouseDown(e.getButton())) {
            mouse.add(e.getButton());
        }
        run.run();
    }

    @Override
    public void mouseReleased(MouseEvent e) {
        removeFromMouse(e.getButton());
        run.run();
    }

    @Override
    public void mouseEntered(MouseEvent e) {
        mouse.clear();
        run.run();
    }

    @Override
    public void mouseExited(MouseEvent e) {
        mouse.clear();
        run.run();
    }

    @Override
    public void mouseDragged(MouseEvent e) {
        mouseX = e.getPoint().x;
        mouseY = e.getPoint().y;
        run.run();
    }

    @Override
    public void mouseMoved(MouseEvent e) {
        mouseX = e.getPoint().x;
        mouseY = e.getPoint().y;
        run.run();
    }

    public int getMouseX() {
        return mouseX;
    }

    public int getMouseY() {
        return mouseY;
    }

    private void runEvent(char c) {
        for (InputTextEvent ev : events) {
            ev.textEnteredEvent(c);
        }
    }

    public void registerTextEvent(InputTextEvent event) {
        this.events.add(event);
    }
}