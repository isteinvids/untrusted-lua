package com.isteinvids.untrusted;

import com.isteinvids.untrusted.level.LevelManager;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.event.ActionEvent;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.AbstractAction;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JSplitPane;
import javax.swing.event.CaretEvent;
import javax.swing.event.CaretListener;
import javax.swing.text.BadLocationException;
import org.fife.ui.rsyntaxtextarea.RSyntaxTextArea;
import org.fife.ui.rsyntaxtextarea.SyntaxConstants;
import org.fife.ui.rsyntaxtextarea.SyntaxScheme;
import org.fife.ui.rsyntaxtextarea.Token;
import org.fife.ui.rtextarea.RTextScrollPane;

/**
 *
 * @author EmirRhouni
 */
public class UntrustedLua {

    private static final int width = 900, height = 650;
    public static final String START = "--%START_EDIT%", END = "--%END_EDIT%";
    public static JFrame mainFrame;
    public static RSyntaxTextArea textArea;
    public static RTextScrollPane textScrollPane;
    public static GameLoop gameLoop;
    public static LevelRenderPanel levelRenderPanel;
    public static LevelManager levelManager;
    public static JSplitPane splitPaneGame;
    public static JPanel computerPane;

    public static void main(String[] args) {
        try {
            mainFrame = new JFrame("Title");
            mainFrame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
            mainFrame.setSize(width, height);
            mainFrame.setResizable(true);
            mainFrame.setLocationRelativeTo(null);
            textArea = new RSyntaxTextArea() {

                @Override
                protected void paintComponent(Graphics g) {
                    String text = this.getText();
                    textArea.removeAllLineHighlights();
                    int i = 0;
                    int caretIndex = getCaretLineNumber();
                    boolean caretIndexInsideEditable = false;
                    boolean insideEditable = false;
                    for (String ln : text.split("\n")) {
                        try {
                            if (ln.contains(END)) {
                                textArea.addLineHighlight(i, Color.lightGray);
                                insideEditable = false;
                            }
                            if (caretIndex == i) {
                                caretIndexInsideEditable = insideEditable;
                            }
                            if (ln.contains(START)) {
                                textArea.addLineHighlight(i, Color.lightGray);
                                insideEditable = true;
                            }
                            if (!insideEditable) {
                                textArea.addLineHighlight(i, Color.lightGray);
                            }
                        } catch (BadLocationException ex) {
                        }
                        i++;
                    }
                    try {
                        if (!insideEditable) {
                            textArea.addLineHighlight(i, Color.lightGray);
                        }
                    } catch (BadLocationException ex) {
                    }
                    textArea.setHighlightCurrentLine(caretIndexInsideEditable);
                    super.paintComponent(g);
                }

            };
            SyntaxScheme def = textArea.getSyntaxScheme();
            def.getStyle(Token.COMMENT_EOL).foreground = Color.lightGray;

//            ((RSyntaxDocument)textArea.getDocument()).setTokenMakerFactory(new AbstractTokenMakerFactory() {
//
//                @Override
//                protected void initTokenMakerMap() {
//                    
//                }
//            });
            textArea.setSyntaxEditingStyle(SyntaxConstants.SYNTAX_STYLE_LUA);
            textArea.setCodeFoldingEnabled(true);
            textArea.setFont(new Font("Monospaced", Font.PLAIN, 12));
            textArea.addFocusListener(new FocusListener() {

                @Override
                public void focusGained(FocusEvent e) {
                    textArea.getCaret().setVisible(true);
                }

                @Override
                public void focusLost(FocusEvent e) {
                    textArea.getCaret().setVisible(true);
                }
            });
            textArea.addCaretListener(new CaretListener() {

                @Override
                public void caretUpdate(CaretEvent e) {
                    int start = textArea.getText().indexOf(START);
                    int end = textArea.getText().indexOf(END);
                    boolean en = false;
                    if (e.getDot() > start + START.length() && e.getDot() < end) {
                        if (e.getMark() > start + START.length() && e.getMark() < end) {
                            en = true;
                        }
                    }
                    textArea.setEditable(en);
                }
            });
            textArea.setEditable(true);
            textScrollPane = new RTextScrollPane(textArea);
            textScrollPane.setPreferredSize(new Dimension(200, height));

            levelRenderPanel = new LevelRenderPanel("script");
            levelRenderPanel.setPreferredSize(new Dimension(width - 300, height));

            gameLoop = new GameLoop(levelRenderPanel);

            JPanel buttonPanel = new JPanel();
            buttonPanel.setPreferredSize(new Dimension(width, 40));

            JButton btnCompile = new JButton("Compile");
            JButton btnReset = new JButton("Reset");
            JButton btnSkipLevel = new JButton("Skip Level");
            btnCompile.addActionListener(new AbstractAction() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    levelManager.runScript(textArea.getText(), false);
                    levelRenderPanel.requestFocus();
                }
            });
            btnReset.addActionListener(new AbstractAction() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    textArea.setText(levelManager.originalScript);
                }
            });
            btnSkipLevel.addActionListener(new AbstractAction() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    levelManager.levelIndex++;
                    levelManager.loadLevel();
                }
            });
            buttonPanel.add(btnCompile);
            buttonPanel.add(btnReset);
            buttonPanel.add(btnSkipLevel);

            computerPane = new JPanel();//JSplitPane(JSplitPane.VERTICAL_SPLIT, true, textScrollPane, buttonPanel);
            splitPaneGame = new JSplitPane(JSplitPane.HORIZONTAL_SPLIT, true, levelRenderPanel, computerPane);
//            computerPane.setDividerLocation(height - 90);
            computerPane.setLayout(new BorderLayout());
            computerPane.add(textScrollPane,BorderLayout.CENTER);
            computerPane.add(buttonPanel,BorderLayout.PAGE_END);
            splitPaneGame.setDividerLocation(width - 350);

            mainFrame.add(splitPaneGame, BorderLayout.CENTER);
            mainFrame.setVisible(true);
//            String levelText = loadLevel(0);
//            textArea.setText(levelText);
            levelManager = new LevelManager();
//            levelManager.runScript(levelText);

            gameLoop.start();
        } catch (Exception ex) {
            Logger.getLogger(UntrustedLua.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public static void showComputerPane(boolean show) {
        UntrustedLua.computerPane.setVisible(show);
        UntrustedLua.computerPane.updateUI();
        UntrustedLua.splitPaneGame.updateUI();
        UntrustedLua.mainFrame.repaint();
//        computerPane.setDividerLocation(height - 80);
        splitPaneGame.setDividerLocation(width - 350);
    }

    private static String loadLevel(int level) throws IOException {
        String res = "/levels/level_" + level + ".lua";
        String ret = "";
        String line;
        InputStream levelStream = UntrustedLua.class.getResourceAsStream(res);
        BufferedReader br = new BufferedReader(new InputStreamReader(levelStream));
        while ((line = br.readLine()) != null) {
            ret += line + "\n";
        }
        br.close();
        return ret;
    }
}
