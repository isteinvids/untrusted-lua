package com.isteinvids.untrusted;

import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.event.MouseEvent;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import javax.imageio.ImageIO;
import javax.swing.JPanel;

/**
 *
 * @author EmirRhouni
 */
public class GameLoop {

    private boolean gameRunning = false;
//    private final JPanel inputPanel = new JPanel)
    private final Input input;
    protected final LevelRenderPanel panel;

    public GameLoop(LevelRenderPanel fr2) {
        panel = fr2;
        input = new Input(null);
    }

    public void addInputToPanel(JPanel panel1) {
        panel1.addKeyListener(input);
        panel1.addMouseListener(input);
        panel1.addMouseMotionListener(input);
    }

    public void start() throws GameException {
        if (!gameRunning) {
            panel.addKeyListener(input);
            panel.addMouseListener(input);
            panel.addMouseMotionListener(input);
            panel.setInput(input);

            panel.init();

            panel.requestFocus();

            gameRunning = true;
            Thread loop = new Thread(new Runnable() {
                @Override
                public void run() {
                    try {
                        gameLoop();
                    } catch (GameException ex) {
                        ex.printStackTrace();
                    }
                }
            }, "GameLoop");
            loop.start();
        }
    }

    //Only run this in another Thread!
    private void gameLoop() throws GameException {
        final double GAME_HERTZ = 30.0;
        final double TIME_BETWEEN_UPDATES = 900000000 / GAME_HERTZ;
        final int MAX_UPDATES_BEFORE_RENDER = 5;
        double lastUpdateTime = System.nanoTime();
        double lastRenderTime = System.nanoTime();

        final double TARGET_FPS = 60;
        final double TARGET_TIME_BETWEEN_RENDERS = 1000000000 / TARGET_FPS;

        int lastSecondTime = (int) (lastUpdateTime / 1000000000);

        while (gameRunning) {
            double now = System.nanoTime();
            int updateCount = 0;

            while (now - lastUpdateTime > TIME_BETWEEN_UPDATES && updateCount < MAX_UPDATES_BEFORE_RENDER && gameRunning) {
                try {
                    panel.update();
                } catch (Exception ex) {
                    System.err.println("Exception in update method");
                ex.printStackTrace();
                }
                if (input.isMousePressed(MouseEvent.BUTTON1)) {
                    panel.requestFocus();
                }
                lastUpdateTime += TIME_BETWEEN_UPDATES;
                updateCount++;
            }

            if (now - lastUpdateTime > TIME_BETWEEN_UPDATES) {
                lastUpdateTime = now - TIME_BETWEEN_UPDATES;
            }
            try {
                panel.repaint();
            } catch (Exception ex) {
                System.err.println("Exception in repaint method");
                ex.printStackTrace();
            }
            lastRenderTime = now;

            int thisSecond = (int) (lastUpdateTime / 1000000000);
            if (thisSecond > lastSecondTime) {
                lastSecondTime = thisSecond;
            }

            while (now - lastRenderTime < TARGET_TIME_BETWEEN_RENDERS && now - lastUpdateTime < TIME_BETWEEN_UPDATES && gameRunning) {
                try {
                    Thread.sleep(2);
                } catch (Exception e) {
                }

                now = System.nanoTime();
            }
        }
    }

    public void drawCenteredString(String text, int x, int y, Graphics2D g) {
        int w = Math.round((float) g.getFont().getStringBounds(text, g.getFontRenderContext()).getWidth());
        Color col = g.getColor();
        g.setColor(Color.black);
        g.drawString(text, x - (w / 1.85f) + 1, y + 1);
        g.setColor(col);
        g.drawString(text, x - (w / 1.85f), y);
    }

    public boolean drawClickableCenteredString(String text, int x, int y, Graphics2D g) {
        return this.drawClickableCenteredString(text, x, y, null, g);
    }

    public boolean drawClickableCenteredString(String text, int x, int y, Color tint, Graphics2D g) {
        int w = Math.round((float) g.getFont().getStringBounds(text, g.getFontRenderContext()).getWidth());
        Color col = g.getColor();
        float nx = x - (w / 1.85f);
        boolean ontop = false;
        boolean ret = false;
        if (input.getMouseX() > x - (w / 2) && input.getMouseX() < x + (w / 2)) {
            if (input.getMouseY() > y - 15 && input.getMouseY() < y + 4) {
                ontop = true;
                if (input.isMousePressed(MouseEvent.BUTTON1)) {
                    ret = true;
                }
            }
        }
        g.setColor(Color.black);
        g.drawString(text, nx + 1, y + 1);
        g.setColor(ontop && tint != null ? tint : col);
        g.drawString(text, nx, y);
        return ret;
    }

    public void drawString(String text, int x, int y, Graphics2D g) {
        Color col = g.getColor();
        g.setColor(Color.black);
        g.drawString(text, x + 1, y + 1);
        g.setColor(col);
        g.drawString(text, x, y);
    }

    public JPanel getPanel() {
        return panel;
    }

    public boolean drawClickableImage(Graphics2D g, Image image, int x, int y) {
        return this.drawClickableImage(g, image, x, y, image.getWidth(null), image.getHeight(null), null);
    }

    public boolean drawClickableImage(Graphics2D g, Image image, int x, int y, Color tint) {
        return this.drawClickableImage(g, image, x, y, image.getWidth(null), image.getHeight(null), tint);
    }

    public boolean drawClickableImage(Graphics2D g, Image image, int x, int y, int width, int height) {
        return this.drawClickableImage(g, image, x, y, width, height, MouseEvent.BUTTON1, null);
    }

    public boolean drawClickableImage(Graphics2D g, Image image, int x, int y, int width, int height, Color tint) {
        return this.drawClickableImage(g, image, x, y, width, height, MouseEvent.BUTTON1, tint);
    }

    public boolean drawClickableImage(Graphics2D g, Image image, int x, int y, int width, int height, int mouseButton, Color tint) {
        g.drawImage(image, x, y, width, height, null);
        if (input.getMouseX() > x && input.getMouseY() > y) {
            if (input.getMouseX() < x + width && input.getMouseY() < y + height) {
                if (tint != null) {
                    g.setColor(tint);
                    g.fillRect(x, y, width, height);
                }
                if (input.isMousePressed(mouseButton)) {
                    return true;
                }
            }
        }
        return false;
    }

    public BufferedImage readImage(String file) throws GameException {
        try {
            File f = new File(file);
            if (f.exists()) {
                return ImageIO.read(f);
            }
            return ImageIO.read(this.getClass().getResourceAsStream("/" + file));
        } catch (IOException ex) {
            throw new GameException(ex);
        }
    }
}
