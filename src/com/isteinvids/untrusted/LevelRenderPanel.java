package com.isteinvids.untrusted;

import com.isteinvids.untrusted.level.Entity;
import com.isteinvids.untrusted.level.Item;
import com.isteinvids.untrusted.level.LevelManager;
import com.isteinvids.untrusted.level.Player;
import com.isteinvids.untrusted.level.Position;
import com.isteinvids.untrusted.level.Tile;
import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Image;
import java.util.Map;
import javax.swing.JPanel;

/**
 *
 * @author EmirRhouni
 */
public class LevelRenderPanel extends JPanel {

    private final String originalScript;
    private String modifiedScript;
    private Image background;
    private Input input;
    private LevelManager levelManager;
    private boolean init = false;

    private Font font;

    public LevelRenderPanel(String originalScript) {
        this.originalScript = originalScript;
    }

    public Input getInput() {
        return input;
    }

    public void setInput(Input input) {
        this.input = input;
    }

    public void setModifiedScript(String modifiedScript) {
        this.modifiedScript = modifiedScript;
    }

    public String getModifiedScript() {
        return modifiedScript;
    }

    public String getOriginalScript() {
        return originalScript;
    }

    public void render(Graphics2D g) {
        font = new Font("Monospaced", Font.PLAIN, 20);
        g.setFont(font);
//        new Font("Courier New", Font.BOLD, 22)
        g.drawImage(background, 0, 0, UntrustedLua.mainFrame.getWidth(), UntrustedLua.mainFrame.getHeight(), this);
        int blocksizex =16;
        int blocksizey =16;

        int offx = (getWidth() / 2) - ((levelManager.getWidth() * blocksizex) / 2), offy = (getHeight() / 2) - ((levelManager.getHeight() * blocksizey) / 2);

        int charheight = g.getFontMetrics().getHeight();
        g.setColor(new Color(levelManager.backgroundColour));
        g.fillRect(offx, offy + (blocksizey / 2) - g.getFontMetrics().getHeight(), (levelManager.getWidth() * blocksizex) + 20, (levelManager.getHeight() * blocksizey) + charheight - 5);

        for (Map.Entry<Position, String> ents : levelManager.tiles.entrySet()) {
            Tile tile = levelManager.blockMapping.get(ents.getValue());
            g.setColor(Color.BLACK);
            g.drawString(Character.toString(tile.getSymbol()), offx + (ents.getKey().x * blocksizex) + 1, offy + (ents.getKey().y * blocksizey) + 1);
            g.setColor(new Color(tile.getColour()));
            g.drawString(Character.toString(tile.getSymbol()), offx + (ents.getKey().x * blocksizex), offy + (ents.getKey().y * blocksizey));
        }

        for (Map.Entry<Position, Integer> ents : levelManager.squareColours.entrySet()) {
            g.setColor(new Color(ents.getValue()));
            g.fillRect(offx + (ents.getKey().x * blocksizex), offy + (ents.getKey().y * blocksizey), blocksizex, blocksizey);
//            g.drawString(Character.toString(tile.c), );
        }

        for (Map.Entry<String, Entity> ents : levelManager.entities.entrySet()) {
            Entity entity = ents.getValue();
            g.setColor(Color.BLACK);
            g.drawString(Character.toString(entity.symbol), offx + (entity.position.x * blocksizex) + 1, offy + (entity.position.y * blocksizey) + 1);
            g.setColor(new Color(entity.colour));
            g.drawString(Character.toString(entity.symbol), offx + (entity.position.x * blocksizex), offy + (entity.position.y * blocksizey));
        }

        if (levelManager.player != null) {
            Player player = levelManager.player;
            int px = levelManager.player.position.x, py = levelManager.player.position.y;
            g.setColor(Color.BLACK);
            g.drawString(Character.toString(player.symbol), offx + (px * blocksizex) + 1, offy + (py * blocksizey) + 1);
            g.setColor(new Color(player.colour));
            g.drawString(Character.toString(player.symbol), offx + (px * blocksizex), offy + (py * blocksizey));
        }

        g.setColor(new Color(0.7F, 0.7F, 0.7F, 0.7F));
        g.fillRect(0, getHeight()-35, getWidth(), 35);

        g.setColor(Color.black);
        g.drawString(levelManager.status, 10 + 1, this.getHeight() - 10 + 1);
        g.setColor(Color.green);
        g.drawString(levelManager.status, 10, this.getHeight() - 10);
        
//        g.drawString(Character.toString((char) 0x2592) + "\u2592", 20, 20);
        int count = 0;
        int invx=getWidth()-30, invy = getHeight()-10;
        for (String invitem : levelManager.playerInventory) {
            Item itm = levelManager.itemMapping.get(invitem);
            if (itm != null) {
                int width = g.getFontMetrics().stringWidth(itm.symbol + "");
//                g.setColor(Color.black);
//                g.drawString(Character.toString(itm.symbol), (this.getWidth() / 2) - (count * (width + 10)) + 1, getHeight() - 10 + 1);
                g.setColor(new Color(itm.colour));
                g.drawString(Character.toString(itm.symbol), invx- (count * (width + 10)), invy);
                count++;
            }
        }

        if (!levelManager.errorText.isEmpty()) {
            int width = g.getFontMetrics().stringWidth(levelManager.errorText);

            g.setColor(new Color(0f, 0f, 0f, 0.8f));
            g.fillRect(0, 0, getWidth(), getHeight());

            g.setColor(Color.black);
            g.drawString(levelManager.errorText, this.getWidth() / 2, this.getHeight() - 10 + 1);
            g.setColor(Color.red);
            g.drawString(levelManager.errorText, (this.getWidth() / 2) - (width / 2), (this.getHeight() / 2) - 30);
        }
    }

    @Override
    protected void paintComponent(Graphics g) {
        g.setColor(Color.black);
        g.fillRect(0, 0, getWidth(), getHeight());
        if (init) {
            render((Graphics2D) g);
        }
    }

    public void init() throws GameException {
//        font = new Font("Courier New", Font.BOLD, 24);
        font = new Font("Monospaced", Font.PLAIN, 24);
        levelManager = UntrustedLua.levelManager;
        background = UntrustedLua.gameLoop.readImage("space.png");
        init = true;
    }

    public void update() {
        levelManager.update(this);
    }

    public void reset() {
    }
}
