package com.isteinvids.untrusted.level;

import org.luaj.vm2.LuaString;
import org.luaj.vm2.LuaTable;
import org.luaj.vm2.LuaValue;
import org.luaj.vm2.lib.BaseLib;
import org.luaj.vm2.lib.TwoArgFunction;
import org.luaj.vm2.lib.ZeroArgFunction;

/**
 *
 * @author EmirRhouni
 */
public class LuaMain extends BaseLib {

    public static enum functions {

        INIT("init"), VALIDATE("validate"), EXIT("exit");
        public final String functionName;

        private functions(String functionName) {
            this.functionName = functionName;
        }
    }
    private final LevelManager level;

    public LuaMain(LevelManager level) {
        this.level = level;
    }

    public static LuaTable positionToTable(Position pos) {
        LuaTable ret = new LuaTable();
        ret.set("x", pos.x);
        ret.set("y", pos.y);
        return ret;
    }

    @Override
    public LuaValue call(LuaValue modname, LuaValue env) {
        LuaTable tc = new LuaTable();
        tc.set("init", new luafunc("init"));
        tc.set("exit", new luafunc("exit", LuaValue.TRUE));
        tc.set("validate", new luafunc("validate"));
        env.set("Main", tc);
        return tc;
    }

    private class luafunc extends ZeroArgFunction {

        private final String funcName;
        private final LuaValue defaultReturn;

        public luafunc(String funcName) {
            this(funcName, LuaValue.NIL);
        }

        public luafunc(String funcName, LuaValue defaultReturn) {
            this.funcName = funcName;
            this.defaultReturn = defaultReturn;
        }

        @Override
        public LuaValue call() {
            System.out.println(funcName);
            return defaultReturn;
        }
    }
}
