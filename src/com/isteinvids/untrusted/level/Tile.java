package com.isteinvids.untrusted.level;

import org.luaj.vm2.LuaFunction;
import org.luaj.vm2.LuaTable;
import org.luaj.vm2.LuaValue;
import org.luaj.vm2.Varargs;

/**
 *
 * @author EmirRhouni
 */
public class Tile {

    private char symbol;
    private int colour;
    private boolean collidable;
    //
    private LuaFunction collideFunc = null;

    public Tile() {
    }

    public Tile(char symbol, int colour, boolean collidable) {
        this.symbol = symbol;
        this.colour = colour;
        this.collidable = collidable;
    }

    public Tile(LuaTable table) {
        this.symbol = table.get("symbol").toString().charAt(0);
        this.colour = table.get("colour").checkint();

        LuaValue collid = table.get("collidable");
        if (collid.isboolean()) {
            this.collidable = table.get("collidable").checkboolean();
        }
        if (collid.isfunction()) {
            this.collideFunc = collid.checkfunction();
        }
    }

    public boolean isCollidable() {
        if (collideFunc != null) {
            Varargs retval = collideFunc.invoke();
            if (!retval.arg1().isboolean()) {
                return true; // make user set a default?
            }
            return retval.arg1().checkboolean();
        }
        return collidable;
    }

    public char getSymbol() {
        return symbol;
    }

    public int getColour() {
        return colour;
    }

}
