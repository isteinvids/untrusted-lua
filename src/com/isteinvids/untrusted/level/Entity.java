package com.isteinvids.untrusted.level;

/**
 *
 * @author EmirRhouni
 */
public class Entity {

    public Position position;
    public char symbol;
    public int colour;
    public boolean collidable;

    public Entity(Position position, char symbol, int colour, boolean collidable) {
        this.position = position;
        this.symbol = symbol;
        this.colour = colour;
        this.collidable = collidable;
    }

}
