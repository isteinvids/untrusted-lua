package com.isteinvids.untrusted.level;

/**
 *
 * @author EmirRhouni
 */
public class Position {

    public int x;
    public int y;

    public Position() {
    }

    public Position(int x, int y) {
        this.x = x;
        this.y = y;
    }

    public void setPosition(int x, int y) {
        this.x = x;
        this.y = y;
    }

    @Override
    public String toString() {
        return "[\"x\": " + x + ", \"y\": " + y + "]";
    }

    @Override
    public boolean equals(Object obj) {
        if (obj instanceof Position) {
            if (((Position) obj).x == this.x && ((Position) obj).y == this.y) {
                return true;
            }
        }
        return false;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 17 * hash + this.x;
        hash = 17 * hash + this.y;
        return hash;
    }
}
