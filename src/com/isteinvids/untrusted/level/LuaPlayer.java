package com.isteinvids.untrusted.level;

import org.luaj.vm2.LuaInteger;
import org.luaj.vm2.LuaTable;
import org.luaj.vm2.LuaValue;
import org.luaj.vm2.Varargs;
import org.luaj.vm2.lib.BaseLib;
import org.luaj.vm2.lib.OneArgFunction;
import org.luaj.vm2.lib.TwoArgFunction;
import org.luaj.vm2.lib.VarArgFunction;

/**
 *
 * @author EmirRhouni
 */
public class LuaPlayer extends BaseLib {

    private final LevelManager level;

    public LuaPlayer(LevelManager level) {
        this.level = level;
    }

    @Override
    public LuaValue call(LuaValue modname, LuaValue env) {
        LuaTable tc = new LuaTable();
        tc.set("move", new move());
        tc.set("killedBy", new killedBy());
        tc.set("init", new init());
        tc.set("hasitem", new hasitem());
        tc.set("takeitem", new takeitem());
        tc.set("getposition", new getposition());
        tc.set("setphonecallback", new setphonecallback());
        env.set("player", tc);
        return tc;
    }

    private class setphonecallback extends OneArgFunction {

        @Override
        public LuaValue call(LuaValue arg) {
            if (arg.isfunction()) {
                level.phoneCallback = level.luaFuncToRunnable(arg.checkfunction());
            }
            return NIL;
        }
    }

    private class takeitem extends OneArgFunction {

        @Override
        public LuaValue call(LuaValue arg) {
            if (arg.isstring()) {
                if (level.playerInventory.contains(arg.toString())) {
                    level.playerInventory.remove(arg.toString());
                }
            }
            return NIL;
        }
    }

    private class hasitem extends OneArgFunction {

        @Override
        public LuaValue call(LuaValue arg) {
            if (arg.isstring()) {
                if (level.playerInventory.contains(arg.toString())) {
                    return TRUE;
                }
            }
            return FALSE;
        }
    }

    private class move extends OneArgFunction {

        @Override
        public LuaValue call(LuaValue arg) {
            if (arg.isstring()) {
                level.movePlayer(arg.toString());
            }
            return NIL;
        }
    }

    private class killedBy extends OneArgFunction {

        @Override
        public LuaValue call(LuaValue arg) {
            if (arg.isstring()) {
                level.setInput(false);
                level.setErrorText("Killed by: " + arg.toString());
                level.player = null;
            }
            return NIL;
        }
    }

    private class getposition extends VarArgFunction {

        @Override
        public Varargs invoke(Varargs args) {
            if (level.player == null) {
                return NIL;
            }
            Position ret = level.player.position;
            return LuaMain.positionToTable(ret);
        }
    }

    private class init extends TwoArgFunction {

        @Override
        public LuaValue call(LuaValue a, LuaValue b) {
            if (a.isint() && b.isint()) {
                if (level.player == null) {
                    level.player = new Player(new Position(a.toint(), b.toint()));
                }
            }
            return LuaValue.NIL;
        }
    }
}
