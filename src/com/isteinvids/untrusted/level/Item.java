package com.isteinvids.untrusted.level;

/**
 *
 * @author EmirRhouni
 */
public class Item {

    public char symbol;
    public int colour;

    public Item() {
    }

    public Item(char symbol, int colour) {
        this.symbol = symbol;
        this.colour = colour;
    }

}
