package com.isteinvids.untrusted;

import java.util.ArrayList;
import org.luaj.vm2.LuaTable;

public class Prim {

    public static int[][] createMaze(int width, int height) {
        // build maze and initialize with only walls
        int[][] maz = new int[width][height];
        for (int x = 0; x < width; x++) {
            maz[x] = new int[height];
            for (int i = 0; i < maz[x].length; i++) {
                maz[x][i]=1;
            }
        }

        // select random point and open as start node
        Point st = new Point((int) (Math.random() * width), (int) (Math.random() * height), null);
        maz[st.r][st.c] = 0;

        // iterate through direct neighbors of node
        ArrayList<Point> frontier = new ArrayList<Point>();
        for (int x = -1; x <= 1; x++) {
            for (int y = -1; y <= 1; y++) {
                if (x == 0 && y == 0 || x != 0 && y != 0) {
                    continue;
                }
                try {
                    if (maz[st.r + x][st.c + y] == 0) {
                        continue;
                    }
                } catch (Exception e) { // ignore ArrayIndexOutOfBounds
                    continue;
                }
                // add eligible points to frontier
                frontier.add(new Point(st.r + x, st.c + y, st));
            }
        }

        Point last = null;
        while (!frontier.isEmpty()) {

            // pick current node at random
            Point cu = frontier.remove((int) (Math.random() * frontier.size()));
            Point op = cu.opposite();
            try {
                // if both node and its opposite are walls
                if (maz[cu.r][cu.c] == 1) {
                    if (maz[op.r][op.c] == 1) {

                        // open path between the nodes
                        maz[cu.r][cu.c] = 0;
                        maz[op.r][op.c] = 0;

                        // store last node in order to mark it later
                        last = op;

                        // iterate through direct neighbors of node, same as earlier
                        for (int x = -1; x <= 1; x++) {
                            for (int y = -1; y <= 1; y++) {
                                if (x == 0 && y == 0 || x != 0 && y != 0) {
                                    continue;
                                }
                                try {
                                    if (maz[op.r + x][op.c + y] == 0) {
                                        continue;
                                    }
                                } catch (Exception e) {
                                    continue;
                                }
                                frontier.add(new Point(op.r + x, op.c + y, op));
                            }
                        }
                    }
                }
            } catch (Exception e) { // ignore NullPointer and ArrayIndexOutOfBounds
            }

            // if algorithm has resolved, mark end node
            if (frontier.isEmpty()) {
                maz[last.r][last.c] = 0;
            }
        }

        // print final maze
        /*for (int i = 0; i < width; i++) {
            for (int j = 0; j < height; j++) {
                if(maz[i][j] == 0)
                System.out.print(' ');
                if(maz[i][j] == 1)
                System.out.print('#');
            }
            System.out.println();
        }*/
        return maz;
    }

    static class Point {

        Integer r;
        Integer c;
        Point parent;

        public Point(int x, int y, Point p) {
            r = x;
            c = y;
            parent = p;
        }

        // compute opposite node given that it is in the other direction from the parent
        public Point opposite() {
            if (this.r.compareTo(parent.r) != 0) {
                return new Point(this.r + this.r.compareTo(parent.r), this.c, this);
            }
            if (this.c.compareTo(parent.c) != 0) {
                return new Point(this.r, this.c + this.c.compareTo(parent.c), this);
            }
            return null;
        }
    }
}
